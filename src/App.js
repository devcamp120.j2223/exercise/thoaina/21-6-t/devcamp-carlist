import { gObjCars, checkCar } from "./info";

function App() {
  return (
    <div className="App">
      <ul>
        {
          gObjCars.map((value, index) => {
            return <li key={index}>{value.make}, biển số {value.vID}, {checkCar(value.year)}</li>
          })
        }
      </ul>
    </div>
  );
}

export default App;
